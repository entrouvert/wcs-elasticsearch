#! /usr/bin/env python

''' Setup script for wcs-es
'''

import os
import subprocess
from setuptools import setup, find_packages
from distutils.command.sdist import sdist as _sdist


class eo_sdist(_sdist):
    def run(self):
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        _sdist.run(self)
        if os.path.exists('VERSION'):
            os.remove('VERSION')


def get_version():
    '''Use the VERSION, if absent generates a version with git describe, if not
       tag exists, take 0.0.0- and add the length of the commit log.
    '''
    if os.path.exists('VERSION'):
        with open('VERSION', 'r') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(['git', 'describe', '--dirty', '--match=v*'], stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        result = p.communicate()[0]
        if p.returncode == 0:
            return result.split()[0][1:].replace('-', '.')
        else:
            return '0.0.0-%s' % len(subprocess.check_output(
                ['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0.0'

setup(name="wcs-es",
      version=get_version(),
      license="AGPLv3 or later",
      description="W.C.S. ElasticSearch feeder",
      long_description=file('README').read(),
      url="http://dev.entrouvert.org/projects/wcs-es/",
      author="Entr'ouvert",
      author_email="info@entrouvert.org",
      include_package_data=True,
      packages=find_packages(),
      install_requires=[
          'requests',
          'elasticsearch',
      ],
      setup_requires=[
      ],
      tests_require=[
          'pytest',
      ],
      dependency_links=[],
      cmdclass={
          'sdist': eo_sdist,
      })
