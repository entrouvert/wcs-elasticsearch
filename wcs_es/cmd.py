from . import wcs_api

def provision(api, es, index_name, recreate=False):
    if recreate and es.indices.exists(index_name):
        es.indices.delete(index_name)
    for formdef in api.get_formdefs():
        es.create(index=index_name, doc_type='formdef', body=formdef.json(), id=formdef.slug)
        try:
            for data in formdef.datas:
                es.create(index_name, doc_type='form-'+formdef.slug, body=data.json(),
                          id=data.display_id)
            print formdef.slug, 'populated'
        except wcs_api.WcsApiError:
            print '!!!', formdef.slug, 'failed'

if __name__ == '__main__':
    import argparse
    import ConfigParser
    import os
    import elasticsearch
    import urlparse

    config = ConfigParser.ConfigParser()
    config_file = os.path.expanduser('~/.wcs_es.ini')
    if os.path.exists(config_file):
        config.read(config_file)
    urls = [url for url in config.sections() if url.startswith('http://') or
            url.startswith('https://')]
    parser = argparse.ArgumentParser(description='Provision ES with W.C.S. data')
    parser.add_argument('--url', help='url of the w.c.s. instance', required=not urls,
                        default=(urls or [None])[0])
    args, rest = parser.parse_known_args()
    defaults = {}
    if getattr(args, 'url') and config.has_section(args.url):
        defaults = dict(config.items(args.url))
    parser.add_argument('--orig', help='origin of the request for signatures',
                        required='orig' not in defaults)
    parser.add_argument('--key', help='HMAC key for signatures', required='key' not in defaults)
    group = parser.add_mutually_exclusive_group(
        required='email' not in defaults and 'name_id' not in defaults)
    group.add_argument('--email', help='email for authentication')
    group.add_argument('--name-id', help='NameID for authentication')
    parser.add_argument('--es-host', help='ElasticSearch hostname', default='localhost')
    parser.add_argument('--es-port', help='ElasticSearch hostname', type=int, default=9200)
    parser.add_argument('--es-no-recreate', help='ElasticSearch hostname', dest='es_recreate',
                        default=True, action='store_false')
    if defaults:
        parser.set_defaults(**defaults)

    args = parser.parse_args()
    api = wcs_api.WcsApi(url=args.url, orig=args.orig, key=args.key, email=args.email,
                         name_id=args.name_id)
    index_name = urlparse.urlparse(args.url).netloc.split(':')[0].replace('.', '_')
    es_hosts = [{'host': args.es_host, 'port': args.es_port, 'use_ssl': False}]
    print es_hosts
    es = elasticsearch.Elasticsearch(es_hosts)
    provision(api, es, index_name, recreate=args.es_recreate)
